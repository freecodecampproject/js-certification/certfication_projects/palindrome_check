const inputText = document.getElementById('text-input')
const checkBtn = document.getElementById('check-btn')
const result = document.getElementById('result')


checkBtn.onclick = checkInput


function checkInput() {
	const inputValue = inputText.value
	const cleanValue = inputValue.toLowerCase().replace(/[^a-z0-9]/g, '')
	const reverseValue = cleanValue.split('').reverse().join('')


	if (inputValue === '') {
		alert('Please input a value')
		return
	}

	if (cleanValue === reverseValue) {
		result.innerHTML = `${inputValue} is a palindrome`
	} else {
		result.innerHTML = `${inputValue} is not a palindrome`
	}

}